﻿GRANT ALL PRIVILEGES ON TABLE FRIEND_REQUESTS TO tfgtest;
GRANT ALL PRIVILEGES ON TABLE GAMEMODES TO tfgtest;
GRANT ALL PRIVILEGES ON TABLE GAME_DATA TO tfgtest;
GRANT ALL PRIVILEGES ON TABLE MATCHES TO tfgtest;
GRANT ALL PRIVILEGES ON TABLE PLAYS TO tfgtest;
GRANT ALL PRIVILEGES ON TABLE REPORTS TO tfgtest;
GRANT ALL PRIVILEGES ON TABLE USERS TO tfgtest;

INSERT INTO GAMEMODES 
(gamemode_name, number_of_teams) VALUES
('T_VS_T__DEATHMATCH',2),
('T_VS_T__CAPTURE_THE_', 2),
('T_VS_T__POINT_OF_CON', 2),
('A_VS_A__DEATHMATCH', 10),
('A_VS_A__VIRUS', 10),
('A_VS_A__SUITCASE', 10),
('A_VS_A__TELEPORT', 10);

INSERT INTO USERS 
(email, username, password, experience, creation_date) VALUES
('', 'Unknown user', '', 0, current_timestamp);

INSERT INTO USERS 
(email, username, password, experience, creation_date, banned_until) VALUES
('email@test', 'Banned', '89b58e1ab7233dd84fe7da8b73cdacb48bed7f29', 0, current_timestamp, current_timestamp),
('email@test2', 'Allowed', '98121f6eef6881e88d43640a7554498c68c69f7c', 0, current_timestamp, null);

INSERT INTO GAME_DATA VALUES
(0, 0, 0, 1);
