-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2016-04-20 21:57:01.661

-- tables
-- Table: FRIEND_REQUESTS
CREATE TABLE FRIEND_REQUESTS (
    from_user int  NOT NULL,
    to_user int  NOT NULL,
    status int  NOT NULL,
    CONSTRAINT FRIEND_REQUESTS_pk PRIMARY KEY (from_user,to_user)
);

-- Table: GAMEMODES
CREATE TABLE GAMEMODES (
    gamemode_id serial  NOT NULL,
    gamemode_name varchar(20)  NOT NULL,
    number_of_teams int  NOT NULL,
    CONSTRAINT GAMEMODES_pk PRIMARY KEY (gamemode_id)
);

-- Table: GAME_DATA
CREATE TABLE GAME_DATA (
    v_major int  NOT NULL,
    v_minor int  NOT NULL,
    v_revision int  NOT NULL,
    exp_rate int  NOT NULL,
    CONSTRAINT GAME_DATA_pk PRIMARY KEY (v_major)
);

-- Table: MATCHES
CREATE TABLE MATCHES (
    match_id serial  NOT NULL,
    start_date timestamp  NOT NULL,
    end_date timestamp  NULL,
    winner_team int  NULL,
    gamemode_id int  NOT NULL,
    CONSTRAINT MATCHES_pk PRIMARY KEY (match_id)
);

-- Table: PLAYS
CREATE TABLE PLAYS (
    user_id int  NOT NULL DEFAULT 0,
    match_id int  NOT NULL,
    personal_score int  NOT NULL DEFAULT 0,
    eliminations int  NOT NULL DEFAULT 0,
    deaths int  NOT NULL DEFAULT 0,
    CONSTRAINT PLAYS_pk PRIMARY KEY (user_id,match_id)
);

-- Table: REPORTS
CREATE TABLE REPORTS (
    report_id serial  NOT NULL,
    from_user int  NOT NULL,
    to_user int  NOT NULL,
    match_id int  NOT NULL,
    reason varchar(150)  NOT NULL,
    report_date timestamp  NOT NULL,
    CONSTRAINT REPORTS_pk PRIMARY KEY (report_id)
);

-- Table: USERS
CREATE TABLE USERS (
    user_id serial  NOT NULL,
    email varchar(20)  NOT NULL,
    username varchar(20)  NOT NULL,
    password char(40)  NOT NULL,
    experience int  NOT NULL DEFAULT 0,
    creation_date timestamp  NOT NULL,
	register_ip cidr  NULL,
    banned_until timestamp  NULL,
    isonline boolean  NOT NULL DEFAULT false,
    CONSTRAINT Email_ak_1 UNIQUE (email) NOT DEFERRABLE  INITIALLY IMMEDIATE,
    CONSTRAINT USERS_pk PRIMARY KEY (user_id)
);

-- foreign keys
-- Reference: FRIEND_REQUESTS_FROM_USERS (table: FRIEND_REQUESTS)
ALTER TABLE FRIEND_REQUESTS ADD CONSTRAINT FRIEND_REQUESTS_FROM_USERS
    FOREIGN KEY (from_user)
    REFERENCES USERS (user_id)
    ON DELETE  CASCADE  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: FRIEND_REQUESTS_TO_USERS (table: FRIEND_REQUESTS)
ALTER TABLE FRIEND_REQUESTS ADD CONSTRAINT FRIEND_REQUESTS_TO_USERS
    FOREIGN KEY (to_user)
    REFERENCES USERS (user_id)
    ON DELETE  CASCADE  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: MATCHES_GAMEMODES (table: MATCHES)
ALTER TABLE MATCHES ADD CONSTRAINT MATCHES_GAMEMODES
    FOREIGN KEY (gamemode_id)
    REFERENCES GAMEMODES (gamemode_id)
    ON DELETE  RESTRICT  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: PLAYS_MATCHES (table: PLAYS)
ALTER TABLE PLAYS ADD CONSTRAINT PLAYS_MATCHES
    FOREIGN KEY (match_id)
    REFERENCES MATCHES (match_id)
    ON DELETE  RESTRICT  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: PLAYS_USERS (table: PLAYS)
ALTER TABLE PLAYS ADD CONSTRAINT PLAYS_USERS
    FOREIGN KEY (user_id)
    REFERENCES USERS (user_id)
    ON DELETE  SET DEFAULT  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: REPORTS_MATCHES (table: REPORTS)
ALTER TABLE REPORTS ADD CONSTRAINT REPORTS_MATCHES
    FOREIGN KEY (match_id)
    REFERENCES MATCHES (match_id)
    ON DELETE  RESTRICT  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: REPORTS_REPORTED_USERS (table: REPORTS)
ALTER TABLE REPORTS ADD CONSTRAINT REPORTS_REPORTED_USERS
    FOREIGN KEY (to_user)
    REFERENCES USERS (user_id)
    ON DELETE  RESTRICT  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: REPORTS_REPORTER_USERS (table: REPORTS)
ALTER TABLE REPORTS ADD CONSTRAINT REPORTS_REPORTER_USERS
    FOREIGN KEY (from_user)
    REFERENCES USERS (user_id)
    ON DELETE  RESTRICT  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- End of file.

